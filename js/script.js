"use strict";
// Permet d'initialiser le fichier script pour exécuter les méthodes
document.addEventListener("DOMContentLoaded", initialiser);


function initialiser(evt) {
    // Récupère les zones utiles pour l'affichage des graphiques et des explications
    var zone_graphique = document.getElementById("zone_graphique");

    // Ajoute un événement de click sur tous les boutons
    document.getElementById("button_audience").addEventListener("click", displayGraphAudience);
    document.getElementById("button_depenses").addEventListener("click", displayGraphDepenses);
    document.getElementById("button_vod").addEventListener("click", displayGraphVOD);
    document.getElementById("button_ouverture").addEventListener("click", displayGraphOuverture);
}

// Fonctions qui permettent d'afficher le graphiques ainsi que son explication
function displayGraphAudience(){
    zone_graphique.innerHTML="";
    var newDiv = document.createElement("div");
    zone_graphique.appendChild(newDiv);
    
    var elementBr = document.createElement("br");
    var elementBr1 = document.createElement("br");
    var elementBr2 = document.createElement("br");
    var elementBr3 = document.createElement("br");
    var elementBr4 = document.createElement("br");
    
    
    var newContent1 = document.createTextNode("Le graphique suivant, montre l'évolution du temps d’écoute de la télévision."); 
    var newContent2 = document.createTextNode("On remarque notamment que les durées d’écoute des 4-14ans et des 15-49 ans ont été divisées par 2 entre 2011 et 2019. On peut supposer que cela est dû à l’arrivée de nouveaux médias comme les sites d’hébergement de vidéos.");
    var newContent3 = document.createTextNode("Enfin, concernant les dépenses en service de vidéo à la demande, elles commencent en 2006 et ne font que croître. Cependant, à partir de 2014 la croissance est vraiment forte et qu’elle dépassera sûrement les dépenses liées au cinéma.");
   
    
    var img = document.createElement("IMG"); 
    img.src = "images/audience_television.png"; 

    newDiv.classList.add("containerFlex");
    newDiv.appendChild(img);
    newDiv.appendChild(newContent1); 
    newDiv.appendChild(elementBr);
    newDiv.appendChild(elementBr3);

    newDiv.appendChild(newContent2); 
    newDiv.appendChild(elementBr1);
    newDiv.appendChild(elementBr4);

    newDiv.appendChild(newContent3); 
    newDiv.appendChild(elementBr2);


    
}

function displayGraphDepenses(){
    zone_graphique.innerHTML="";
    var newDiv = document.createElement("div");
    zone_graphique.appendChild(newDiv);
    
    var elementBr = document.createElement("br");
    var elementBr1 = document.createElement("br");
    var elementBr2 = document.createElement("br");
    var elementBr3 = document.createElement("br");
    var elementBr4 = document.createElement("br");
    var elementBr5 = document.createElement("br");
    
    
    var newContent1 = document.createTextNode('A l’aide de ce graphique nous pouvons voir l’évolution des dépenses des ménages en programmes audiovisuels en fonction des années.'); 
    var newContent2 = document.createTextNode('Tout d’abord, quand on s’intéresse aux dépenses en vidéo physique. Ce qui est le plus flagrant sur cette courbe est le pic présent et la chute qui suit ce pic car suite à cela, les dépenses ne font que diminuer.')
    var newContent3 = document.createTextNode('Ensuite, nous allons nous intéresser aux dépenses dans le cinéma. On remarque des légers pics assez réguliers et même si les dépenses diminuent par rapport à l’année précédente, on retrouve une croissance positive au fil des années. On peut expliquer ces piques par rapport à l’attente de certains films lors de certaines années. Par exemple en 2019 est sorti le film Avenger: End Game étant très attendu, il est devenu le film le plus vu de tous les temps.');
    var newContent4 = document.createTextNode('Enfin, concernant les dépenses en service de vidéo à la demande, elles commencent en 2006 et ne font que croître. Cependant, à partir de 2014 la croissance est vraiment forte et qu’elle dépassera sûrement les dépenses liées au cinéma.');
   
    
    var img = document.createElement("IMG"); 
    img.src = "images/depenses_menages_programmes_audiovisuels.PNG"; 

    newDiv.classList.add("containerFlex");
    newDiv.appendChild(img);
    newDiv.appendChild(newContent1); 
    newDiv.appendChild(elementBr);
    newDiv.appendChild(elementBr3);
    newDiv.appendChild(newContent2); 
    newDiv.appendChild(elementBr1);
    newDiv.appendChild(elementBr4);
    newDiv.appendChild(newContent3); 
    newDiv.appendChild(elementBr2);
    newDiv.appendChild(elementBr5);
    newDiv.appendChild(newContent4); 



    

    
}

function displayGraphVOD(){
    zone_graphique.innerHTML="";
    var newDiv = document.createElement("div");
    zone_graphique.appendChild(newDiv);

    var elementBr = document.createElement("br");
    var elementBr1 = document.createElement("br");
    var elementBr2 = document.createElement("br");
    var elementBr3 = document.createElement("br");
    var elementBr4 = document.createElement("br");
    var elementBr5 = document.createElement("br");
    var elementBr6 = document.createElement("br");
    var elementBr7 = document.createElement("br");
    
    
    var newContent1 = document.createTextNode('Ce graphique correspond au pourcentage de la consommation de la VOD en fonction des années. Pour ce graphique la VOD ne prend pas en compte les services de SVOD. Avant 2009 la VOD était très peu présente dans les ménages. On peut voir que jusqu’à 2015 les 15-24 ans consomment pas mal de VOD tandis que les 50 ans et plus en consomment assez peu. A partir de 2016 cette tendance s’inverse et les 15-24 ans font partis de ceux qui consomment le moins la VOD et les personnes de 50 ans et + en consomment le plus.'); 
    var newContent2 = document.createTextNode('En ce qui concerne les personnes de 25 – 34 ans leur consommation ne cesse de diminuer tandis que les 35 – 49 ans consomment de plus en plus de VOD mais à partir de 2014 cette consommation diminue.')
    var newContent3 = document.createTextNode('A partir de 2016 la tendance s’inverse puisque les 50 ans et plus deviennent les plus gros consommateurs de VOD.');
    var newContent4 = document.createTextNode('On peut expliquer cette évolution par des suppositions. Tout d’abord en ce qui concerne les 15-49 ans, on peut dire qu’ils consomment moins de VOD à cause de l’apparition et du boom des services de SVOD, notamment avec l’arrivée de Netflix puis toutes les plateformes qui ont suivies ce modèle. Les plus jeunes générations sont plus favorables au large choix proposé par la SVOD (grand nombre de séries, de films etc…). ');
    var newContent5 = document.createTextNode('En ce qui concerne les personnes de 50 ans et plus, cela prend en compte également les personnes âgées, qui elles sont habituées à regarder la télé. En fait ce que l’on peut se dire c’est que les box internet proposent aujourd’hui tous des services de VOD mais également la télé, ces box se sont généralisées en proposant un accès simple à la VOD. Ces générations apprécient le fait de pouvoir acheter / louer un film puis le regarder, sans forcément avoir la toute dernière série du moment. ');
    
    var img = document.createElement("IMG"); 
    img.src = "images/pourcentage_consommation_vod.PNG"; 

    newDiv.classList.add("containerFlex");
    newDiv.appendChild(img);
    newDiv.appendChild(newContent1); 
    newDiv.appendChild(elementBr);
    newDiv.appendChild(elementBr1);
    newDiv.appendChild(newContent2); 
    newDiv.appendChild(elementBr2);
    newDiv.appendChild(elementBr3);
    newDiv.appendChild(newContent3); 
    newDiv.appendChild(elementBr4);
    newDiv.appendChild(elementBr5);
    newDiv.appendChild(newContent4); 
    newDiv.appendChild(elementBr6);
    newDiv.appendChild(elementBr7);
    newDiv.appendChild(newContent5); 

}



function displayGraphOuverture(){
    zone_graphique.innerHTML="";
    zone_graphique.innerHTML="";
    var newDiv = document.createElement("div");
    zone_graphique.appendChild(newDiv);
    var elementBr = document.createElement("br");
    var elementBr1 = document.createElement("br");
    var elementBr2 = document.createElement("br");
    var elementBr3 = document.createElement("br");
    
    
    var newContent1 = document.createTextNode('Nous voulions finir notre rapport sur un sujet qui nous intéresse fortement : l’impact du COVID-19 sur l’audiovisuel. Comme nous avons pu le voir sur les graphiques précédents, le cinéma n’a jamais été impacté par les vidéos physiques ou par la vidéo à la demande. Cependant pour cette année 2020 les données du cinéma vont très certainement baissées fortement car les cinémas auront été fermés sur presque 6 mois. On se doute donc que la population se sera donc tournée vers les différentes plateformes de vidéo à la demande. '); 
    var newContent2 = document.createTextNode('De plus nous avons très peur pour la pérennité des cinémas, aujourd’hui les gros films n’ont plus peur de ne pas sortir au cinéma, la quasi-totalité les films importants de l’année 2020 ont été repoussés ou alors vont sortir ou sont sortis sur des plateformes de streaming, ceci est donc très problématique pour l’avenir du cinéma. ')
    var newContent3 = document.createTextNode('Si les producteurs sont satisfaits de leur diffusion sur les plateformes sommes nous en train de dire au revoir au cinéma ? ');

    var newDiv2 = document.createElement("div");
    newDiv2.classList.add("ouverture");
    newDiv.classList.add("containerOuverture");

    newDiv.appendChild(newContent1); 
    newDiv.appendChild(elementBr);
    newDiv.appendChild(elementBr1);
    newDiv.appendChild(newContent2); 
    newDiv.appendChild(elementBr2);
    newDiv.appendChild(elementBr3);
    newDiv.appendChild(newDiv2); 
    newDiv2.appendChild(newContent3); 
     

}


